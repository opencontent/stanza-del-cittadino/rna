FROM python:3.7.9-alpine

ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache tzdata

ENV TZ=Europe/Rome

RUN mkdir -p /app

WORKDIR /app

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN chmod +x /app/*.py

CMD [ "/app/rna.py" ]
