#!/usr/bin/env python
import copy
import datetime
import json
import os
import traceback
from time import sleep

from utils import utils
from utils.logger import logger
from utils.rna import RNA
from utils.sdc import SDC
from utils.utils import unflatten_submission, merge_dicts

from dotenv import load_dotenv

load_dotenv()

# Environment variables

# RNA
RNA_USERNAME = os.environ.get("RNA_USERNAME", None)
RNA_PASSWORD = os.environ.get("RNA_PASSWORD", None)
RNA_ENDPOINT = os.environ.get("RNA_ENDPOINT", None)
RNA_CERTIFICATE = os.environ.get("RNA_CERTIFICATE", "./config/cert.pem")
RNA_ATTEMPTS = int(os.environ.get("RNA_ATTEMPTS", 10))
RNA_ERROR_SLEEP_SECONDS = int(os.environ.get("RNA_ERROR_SLEEP_SECONDS", 60))

RNA_SUCCESS_SLEEP_SECONDS = int(os.environ.get("RNA_SUCCESS_SLEEP_SECONDS", 1))
RNA_REGISTRA_AIUTO_SLEEP_SECONDS = int(os.environ.get("RNA_REGISTRA_AIUTO_SLEEP_SECONDS", 4))

# SDC
SDC_ENDPOINT = os.environ.get("SDC_ENDPOINT", None)
SDC_USERNAME = os.environ.get("SDC_USERNAME", None)
SDC_PASSWORD = os.environ.get("SDC_PASSWORD", None)
SDC_SERVICE_ID = os.environ.get("SDC_SERVICE_ID", None)
SDC_BACKOFFICE_URL = os.environ.get("SDC_BACKOFFICE_URL", None)
SDC_STATUSES = os.environ.get("SDC_STATUSES", "status_submitted|status_pending")
SDC_STATUSES = SDC_STATUSES.split("|") if SDC_STATUSES else None
SDC_API_OFFSET = int(os.environ.get("SDC_API_OFFSET", 0))
SDC_API_LIMIT = int(os.environ.get("SDC_API_LIMIT", 100))
SDC_IDS = os.environ.get("SDC_IDS", None)
SDC_IDS = SDC_IDS.split("|") if SDC_IDS else None
SDC_START_DATE = os.environ.get("SDC_START_DATE", None)
SDC_END_DATE = os.environ.get("SDC_END_DATE", None)
SDC_APPLICATIONS_LIMIT = int(os.environ.get("SDC_APPLICATIONS_LIMIT", -1))
SDC_ATTEMPTS = int(os.environ.get("SDC_ATTEMPTS", 10))
SDC_ERROR_SLEEP_SECONDS = int(os.environ.get("SDC_ERROR_SLEEP_SECONDS", 60))

RNA_BATCH_LIMIT = int(os.environ.get("RNA_BATCH_LIMIT", 1))

sdc = None
rna = None

try:
    rna = RNA(
        api_url=RNA_ENDPOINT,
        username=RNA_USERNAME,
        password=RNA_PASSWORD,
        certificate=RNA_CERTIFICATE,
        attempts=RNA_ATTEMPTS,
        sleep_seconds=RNA_ERROR_SLEEP_SECONDS
    )

    sdc = SDC(
        api_url=SDC_ENDPOINT,
        username=SDC_USERNAME,
        password=SDC_PASSWORD,
        service_id=SDC_SERVICE_ID,
        backoffice_form_url=SDC_BACKOFFICE_URL,
        statuses=SDC_STATUSES,
        start_date=SDC_START_DATE,
        end_date=SDC_END_DATE,
        ids=SDC_IDS,
        api_offset=SDC_API_OFFSET,
        api_limit=SDC_API_LIMIT,
        attempts=SDC_ATTEMPTS,
        sleep_seconds=SDC_ERROR_SLEEP_SECONDS,
        limit=SDC_APPLICATIONS_LIMIT,
    )

    applications = sdc.get_applications()
 
    applications_count = len(applications)
    logger.info(f"BEGIN LOOP on {applications_count} applications")

    loop_counter = 0

    for application in applications:
        registra_aiuto = False
        application_id = application['id']
        backoffice_data = application["backoffice_data"]

        # skip application with no backoffice data
        if not backoffice_data or '':
            logger.info(f"SKIP APPLICATION {application_id}: missing backoffice data")
            continue

        backoffice_data = unflatten_submission(backoffice_data)

        if 'rna' not in backoffice_data or 'data' not in backoffice_data['rna']:
            logger.error(f"SKIP APPLICATION {application_id}: rna data missing or sending to RNA not enabled")
            continue

        backoffice_data = unflatten_submission(backoffice_data)['rna']['data']

        # skip application if send flag is not activated
        if not backoffice_data['send_rna']:
            logger.info(f"SKIP APPLICATION {application_id}: sending registration to RNA not enabled")
            continue

        submission = merge_dicts(
            dict1=unflatten_submission(application["backoffice_data"]),
            dict2=sdc.backoffice_data
        )

        # Registra aiuto
        if backoffice_data['status'] == 0:
            logger.info(f"REGISTRA AIUTO APPLICATION {application_id}")


            #if (submission["rna"]["data"]["cor"])
            #    ROMPITI

            registra_aiuto = True

            # questa e' la variabile che ha i placeholder direttamente dal config
            # ne facciamo copia per riusarla in questo batch
            registra_aiuto_copy = dict()
            registra_aiuto_copy = copy.deepcopy(utils.registra_aiuto)
            registra_aiuto_data = dict()
            #logger.debug(f"id {application_id}: valore di CONFIG ID_CONCESSIONE_GEST '{CONFIG['ID_CONCESSIONE_GEST']}'")
            registra_aiuto_data = sdc.populate_data(application, registra_aiuto_copy)
            response = rna.registra_aiuto(registra_aiuto_data)

            if str(response["retCode"]) == "0":
                submission["rna"]["data"]["status"] = 1
                submission["rna"]["data"]["request_id"] = response["idRichiesta"]
                submission["rna"]["data"]["request_status"] = None
                logger.info(f"REGISTRA AIUTO APPLICATION {application_id}: success idRichiesta {response['idRichiesta']}, move to status 1")
            else:
                submission["rna"]["data"]["request_status"] = f"{response['retMessage']} [{response['retCode']}]"
                logger.info(f"REGISTRA AIUTO APPLICATION {application_id}: failure with error {response['retMessage']} {response['retCode']}")

        # Stato richiesta: polling della richiesta fatta in precedenza
        elif backoffice_data['status'] in [1, 4]:
            logger.info(f"STATO RICHIESTA APPLICATION {application_id}")

            stato_esito_richiesta_copy = copy.deepcopy(utils.stato_esito_richiesta)
            stato_esito_richiesta = sdc.populate_data(application, stato_esito_richiesta_copy)
            response = rna.stato_richiesta(stato_esito_richiesta)

            if str(response["retCode"]) == "0":
                submission["rna"]["data"]["request_status"] = None
            if response["retMessage"] == "Completata":
                submission["rna"]["data"]["status"] += 1
                logger.info(f"STATO RICHIESTA APPLICATION {application_id}: success, move to status {submission['rna']['data']['status']}")

        # Scarica richiesta
        elif backoffice_data['status'] in [2, 5]:
            logger.info(f"SCARICA RICHIESTA APPLICATION {application_id}")
            stato_esito_richiesta_copy = copy.deepcopy(utils.stato_esito_richiesta)
            stato_esito_richiesta = sdc.populate_data(application, stato_esito_richiesta_copy)
            response = rna.scarica_esito_richiesta(stato_esito_richiesta)

            batch_outcome = response["esito"]["OUTPUT_RICHIESTA"]

            if str(batch_outcome["ESITO"]["CODICE"]) == "0":
                # No batch error

                outcome = batch_outcome["ESITO_RICHIESTA"]["ESITI_RICH_CONCESSIONI"]["ESITO_RICH_CONCESSIONE"][0]

                # Check single help request outcome
                if str(outcome["ESITO"]["CODICE"]) == "0":
                    if backoffice_data['status'] == 2:
                        # Update COR for aid request only
                        submission["rna"]["data"]["cor"] = outcome["COR"]
                    submission["rna"]["data"]["status"] += 1
                    submission["rna"]["data"]["request_status"] = None

                    if submission["rna"]["data"]["status"] == 3:
                        logger.info(f"SCARICA RICHIESTA APPLICATION {application_id}: success, COR is {outcome['COR']}, move to status 3")
                    elif submission["rna"]["data"]["status"] == 6:
                        logger.info(f"SCARICA RICHIESTA APPLICATION {application_id}: success, request COMPLETED, move to status 6")
                else:
                    # Non dovrebbe arrivare
                    submission["rna"]["data"]["send_rna"] = False
                    submission["rna"]["data"]["request_status"] = f'{outcome["ESITO"]["DESCRIZIONE"]} [{outcome["ESITO"]["CODICE"]}]'
                    logger.error(f"SCARICA RICHIESTA APPLICATION {application_id}: condizione imprevista, disattivato invio a RNA per questa pratica")

            else:
                # An error occurred in the whole batch
                error_msg = f'{batch_outcome["ESITO"]["DESCRIZIONE"]} [codice {batch_outcome["ESITO"]["CODICE"]}]'

                outcome = None
                if "ESITI_RICH_CONCESSIONI" in batch_outcome["ESITO_RICHIESTA"]:
                    outcome = batch_outcome["ESITO_RICHIESTA"]["ESITI_RICH_CONCESSIONI"]["ESITO_RICH_CONCESSIONE"][0]
                    error_msg = f'{outcome["ESITO"]["DESCRIZIONE"]} [codice {batch_outcome["ESITO"]["CODICE"]}]'
                elif "ESITI_CONCESSIONI_RAW" in batch_outcome["ESITO_RICHIESTA"]:
                    outcome = batch_outcome["ESITO_RICHIESTA"]["ESITI_CONCESSIONI_RAW"]["ESITO_CONCESSIONE_RAW"][0]

                if outcome:
                    errors = []
                    if 'ESITO_VERIFICHE_APPLICATIVE' in outcome:
                        verifiche_applicative = outcome['ESITO_VERIFICHE_APPLICATIVE']

                        if 'CONCESSIONE_ESITI' in verifiche_applicative and 'ESITI' in verifiche_applicative[
                            'CONCESSIONE_ESITI'] \
                                and 'ESITO' in verifiche_applicative['CONCESSIONE_ESITI']['ESITI']:
                            for esito_concessione in verifiche_applicative['CONCESSIONE_ESITI']['ESITI']['ESITO']:
                                if esito_concessione['CODICE'] == 'KO':
                                    errors.append(esito_concessione['DESCRIZIONE'])

                        if 'COMPONENTI_AIUTI' in verifiche_applicative and 'COMPONENTE_AIUTO' in verifiche_applicative[
                            'COMPONENTI_AIUTI'] and 'ESITI' in \
                                verifiche_applicative['COMPONENTI_AIUTI']['COMPONENTE_AIUTO'][0] \
                                and 'ESITO' in verifiche_applicative['COMPONENTI_AIUTI']['COMPONENTE_AIUTO'][0][
                            'ESITI']:
                            for esito_aiuto in \
                            verifiche_applicative['COMPONENTI_AIUTI']['COMPONENTE_AIUTO'][0]['ESITI']['ESITO']:
                                if esito_aiuto['CODICE'] == 'KO':
                                    errors.append(esito_aiuto['DESCRIZIONE'])

                    if "ESITI" in outcome and "ESITO" in outcome["ESITI"]:
                        for esito_aiuto in outcome['ESITI']['ESITO']:
                            errors.append(esito_aiuto['DESCRIZIONE'])

                    if len(errors) > 0:
                        error_msg += '\n\n'
                        for error in errors:
                            error_msg += f'- {error}\n'

                submission["rna"]["data"]["send_rna"] = False
                submission["rna"]["data"]["request_status"] = error_msg

        # Conferma concessione
        # FIXME: resolution_date e' doppio!!!!!!!!!!!!!!!!!!!!!!1
        elif backoffice_data['status'] == 3 and backoffice_data["cor"] and backoffice_data["resolution_date"] and backoffice_data["resolution_number"]:
            if not backoffice_data['send_confirm']:
                logger.info(f"SKIP APPLICATION {application_id}: sending confirmation to RNA not enabled")
                continue

            logger.info(f"RICHIESTA CONFERMA CONCESSIONE APPLICATION {application_id}")

            submission["rna"]["data"]["request_status"] = None
            conferma_concessione_copy = copy.deepcopy(utils.conferma_concessione)
            conferma_concessione = sdc.populate_data(application, conferma_concessione_copy)
            response = rna.conferma_concessione(conferma_concessione)

            if response["success"]:
                submission["rna"]["data"]["status"] = 4
                submission["rna"]["data"]["request_status"] = None
        else:
            logger.info(f"SKIP APPLICATION {application_id}: nothing to do")
            continue

        submission["rna"]["data"]["last_update"] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f%z")

        submission = {"backoffice_data": submission}

        application = sdc.update_backoffice_data(application_id=application['id'], payload=submission)
        logger.debug(f"Updated data: {json.dumps(application['backoffice_data'], indent=2)}")

        sleep(RNA_SUCCESS_SLEEP_SECONDS)
        if registra_aiuto:
            sleep(RNA_REGISTRA_AIUTO_SLEEP_SECONDS)
        
        loop_counter+=1
        if (loop_counter >= RNA_BATCH_LIMIT):
            logger.info(f"BATCH LIMIT reached, processed {loop_counter}/{applications_count} applications")
            exit(0)

    logger.info(f"END LOOP on {applications_count} applications")

except KeyError as ex:
    logger.error(f"Unrecognized response format: {str(ex)}")
except Exception as ex:
    logger.error(str(ex))
    logger.debug(traceback.print_exc() or "No traceback available")
    exit(1)
