import json
from json import JSONDecodeError
from time import sleep
import requests

from utils.logger import logger


class RNAException(Exception):
    pass


class RNA:
    def __init__(self, api_url, username, password, certificate, attempts=10, sleep_seconds=60):
        logger.debug(f"Initialize RNA Class")

        self.api_url = api_url
        self.username = username
        self.password = password
        self.certificate = certificate
        self.attempts = attempts
        self.sleep_seconds = sleep_seconds

        self.validate()

    def validate(self):
        """
        Validates input data
        """
        if not self.api_url:
            raise RNAException("Missing endpoint")
        if not self.username:
            raise RNAException("Missing username")
        if not self.password:
            raise RNAException("Missing password")
        if not self.certificate:
            raise RNAException("Missing certificate")

    def safe_request(self, url, method="POST", payload=None):
        """
        Method for handling possible errors that may be encountered in GET a http request
        :param payload: payload of the request (optional)
        :param method: http method (optional, default POST)
        :param url: url to request (required)
        :return: Response data
        """
        attempt = 0

        # Retry request for #attempts

        headers = {"Content-Type": "application/json"}

        while attempt < self.attempts:
            logger.debug(f"attempt {attempt + 1}/{self.attempts}: {method}: {url}")
            logger.debug(f"HEADERS: {json.dumps(headers, indent=2)}")
            logger.debug(f"DATA: {json.dumps(payload, indent=2)}")

            attempt += 1
            response = requests.request(
                method=method,
                url=url,
                data=json.dumps(payload),
                headers=headers,
                auth=(self.username, self.password),
                cert=self.certificate,
                verify=False
            )
            try:
                if response.ok:
                    logger.debug(f"RESPONSE: {json.dumps(response.json(), indent=2)}")
                    return response.json()
                elif response.status_code == 401:
                    # If token is expired, request a new one
                    raise RNAException(
                        f"RNA: {response.status_code} bad credentials while requesting {method} {url}")
                elif response.status_code in range(400, 600):
                    error = json.dumps(response.json())
                    raise RNAException(f"RNA: {response.status_code} error while requesting {method} {url}:{error}")
            except JSONDecodeError:
                raise RNAException(f"RNA: Response is not a valid JSON: {response.text}")

            sleep(self.sleep_seconds * 2 ^ attempt)

        raise RNAException(f"RNA: cannot complete request  {method} {url} after {attempt} attempts")

    def registra_aiuto(self, data):
        """
        Performs the aid registration request
        :param data: payload of the request (required)
        :return: registraaiuto response
        """
        url = f"{self.api_url}/rna/registraaiuto"
        return self.safe_request(url=url, payload=data)["Result"]

    def stato_richiesta(self, data):
        """
        Performs the request to retrieve the status of the request
        :param data: payload of the request (required)
        :return: statorichiesta response
        """
        url = f"{self.api_url}/rna/statorichiesta"
        return self.safe_request(url=url, payload=data)["return"]

    def scarica_esito_richiesta(self, data):
        """
        Performs the request to retrieve request outcome
        :param data: payload of the request (required)
        :return: scaricaesitorichiesta response
        """
        url = f"{self.api_url}/rna/scaricaesitorichiesta"
        return self.safe_request(url=url, payload=data)["return"]

    def conferma_concessione(self, data):
        """
        Performs the request to confirm the concession of the financial aid
        :param data: payload of the request (required)
        :return: confermaconcessionemassiva response
        """
        url = f"{self.api_url}/rna/confermaconcessionemassiva"
        return self.safe_request(url=url, payload=data)["return"]
