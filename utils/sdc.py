import datetime
import json
import os
import re
from json import JSONDecodeError
from time import sleep
import requests

from utils.utils import DATE_FMT
from utils.logger import logger

import dotenv

dotenv.load_dotenv()
DEFAULT_PLACEHOLDERS_PATH = os.environ.get("DEFAULT_PLACEHOLDERS_PATH", "./config/default_placeholders.json")

DEFAULT_PLACEHOLDERS = json.load(open(DEFAULT_PLACEHOLDERS_PATH))

class SDCException(Exception):
    pass


class SDC:
    def __init__(self, api_url, username, password, service_id, backoffice_form_url, api_offset=0,
                 api_limit=100, attempts=10, sleep_seconds=60,
                 statuses=None, ids=None, start_date=None, end_date=None, limit=None):
        logger.debug(f"Initialize SDC Class")

        if ids is None:
            ids = []
        if statuses is None:
            statuses = []

        self.api_url = api_url
        self.username = username
        self.password = password
        self.api_offset = api_offset
        self.api_limit = api_limit
        self.statuses = statuses
        self.service_id = service_id
        self.backoffice_form_url = backoffice_form_url
        self.ids = ids
        self.start_date = start_date
        self.end_date = end_date
        self.attempts = attempts
        self.limit = limit
        self.sleep_seconds = sleep_seconds

        self.token = None
        self.service = None
        self.backoffice_data = None
        self.backoffice_mapping = None

        self.validate()

        self.set_token()
        self.set_service()
        self.get_backoffice_schema()

    def validate(self):
        """
        Validates input data
        """
        if not self.api_url:
            logger.error("Missing endpoint")
            raise SDCException("Missing endpoint")
        if not self.username:
            logger.error("Missing username")
            raise SDCException("Missing username")
        if not self.password:
            logger.error("Missing password")
            raise SDCException("Missing password")
        if not self.service_id:
            logger.error("Missing service identifier")
            raise SDCException("Missing service identifier")
        if not self.backoffice_form_url:
            logger.error("Missing backoffice form")
            raise SDCException("Missing backoffice form")

    def set_token(self):
        """
        Method for generating an authentication token
        :return: "SDC Bearer token"
        """

        data = {
            "username": self.username,
            "password": self.password
        }

        response = requests.post(
            url=f"{self.api_url}/auth",
            data=json.dumps(data)
        )

        try:
            if response.ok:
                self.token = f"Bearer {response.json()['token']}"
            elif response.status_code == 401:
                # If token is expired, request a new one
                raise SDCException("SDC: Invalid credentials")
            elif response.status_code in range(400, 600):
                raise SDCException(
                    f"SDC: Get {response.status_code} error while retrieving token:\n{json.dumps(response.json(), indent=4)}"
                )
        except JSONDecodeError:
            raise SDCException(f"SDC: Response is not a valid JSON: {response.text}")

    def set_service(self):
        """
        Method for generating an authentication token
        :return: "SDC Bearer token"
        """

        service = self.get_service(self.service_id)

        if not service:
            raise SDCException(
                f"SDC: Invalid service id {self.service_id}"
            )
        self.service = service

    def replace_placeholders(self, string, application, uppercase=True):
        """
        Method for replacing placeholders with specific fields of an application
        :param uppercase: convert replaced data in uppercase: default True. NB only data extracted from form data will be converted
        :param string: Phrase containing placeholders
        :param application: Application from which to obtain the values of the placeholders to be replaced
        :return: Initial phrase to which the placeholders have been replaced (if a matching has been found in the
        application)
        """
        # Get placeholders inside phrase
        application_id = application['id']
        placeholders = re.findall(r"%[A-Za-z.\-_1-9|]+%", string)

        logger.debug(f"id {application_id}: checking placeholders ")

        for placeholder in placeholders:
            found = False
            clean_placeholder = placeholder.replace("%", "")

            logger.debug(f"id {application_id}: check placeholder {placeholder}")

            value = ""

            if application['id'] in DEFAULT_PLACEHOLDERS and clean_placeholder in DEFAULT_PLACEHOLDERS[application_id]:
                value = str(DEFAULT_PLACEHOLDERS[application_id][clean_placeholder])
                logger.debug(f"id {application_id}: check placeholder {placeholder}: got value '{value}' from defaults file")
                found = True

            # Search placeholder value in application module keys
            elif clean_placeholder.startswith("form."):
                _placeholder = clean_placeholder.replace("form.", "")
                if _placeholder in application["data"]:
                    value = str(application["data"][_placeholder])
                    found = True
                    logger.debug(f"id {application_id}: check placeholder {placeholder}: got value '{value}' from application[data]")

            # Search placeholder value in application keys
            elif clean_placeholder.startswith("application."):
                _placeholder = clean_placeholder.replace("application.", "")
                if _placeholder in application:
                    value = str(application[_placeholder])
                    found = True
                    logger.debug(f"id {application_id}: check placeholder {placeholder}: got value '{value}' from application")

            # Search placeholder value in application module keys
            elif clean_placeholder.startswith("backoffice."):
                _placeholder = clean_placeholder.replace("backoffice.", "")
                if _placeholder in application["backoffice_data"]:
                    value = str(application["backoffice_data"][_placeholder])
                    found = True
                    logger.debug(f"id {application_id}: check placeholder {placeholder}: got value '{value}' from backoffice_data")

            # Search placeholder value in application authentication keys
            elif clean_placeholder.startswith("authentication."):
                _placeholder = clean_placeholder.replace("authentication.", "")
                if _placeholder in application["authentication"]:
                    value = str(application["authentication"][_placeholder])
                    found = True

            # Conventions
            if clean_placeholder == "application.submitted_at":
                date = datetime.datetime.strptime(application["submitted_at"], "%Y-%m-%dT%H:%M:%S%z")
                value = date.strftime(DATE_FMT)
                found = True

            if clean_placeholder == "current_date":
                date = datetime.datetime.now()
                value = date.strftime(DATE_FMT)
                found = True


            value = value.upper() if uppercase else value
            string = string.replace(placeholder, value)

            if not found:
                raise SDCException(f"SDC: placeholder {placeholder} not found")

            # Remove double spaces
            string = string.replace("  ", " ")

        return string

    # data e' la struttura da inviare a RNA che e' fatta a tanti livelli annidati
    def populate_data(self, application, data):
        if type(data) != dict:
            return data
        for key, val in data.items():
            if type(val) == dict:
                data[key] = self.populate_data(application, val)
            elif type(val) == list:
                tmp_list = []
                for _ in val:
                    tmp_list.append(self.populate_data(application, _))
                data[key] = tmp_list
            else:
                data[key] = self.replace_placeholders(str(val) if val else "", application, False)
                logger.debug(f"id {application['id']}: replace placeholder in {key}, change {val} in {data[key]}")
        return data

    def extract_schema(self, schema):
        """
        Recursively extrapolate the schema structure of the json form
        :param schema: current scheme
        :return: outline of the form structure
        """
        data = {}
        if 'label' in schema:
            return False if schema['type'] == 'checkbox' else None
        for key in schema.keys():
            data[key] = self.extract_schema(schema[key])
        return data

    def extract_mapping(self, schema, prev_key):
        """
        Generates a mapping between the form schema and the application submission data
        :param schema: form schema
        :param prev_key: top level key, it is a necessary parameter for retrieving the structure of a nested form
        :return: submission-schema mapping dictionary
        """
        data = {}
        if 'label' in schema:
            return prev_key
        for key in schema.keys():
            if key == 'data':
                new_key = prev_key
            elif not prev_key:
                new_key = key
            else:
                new_key = f"{prev_key}.{key}"
            data[key] = self.extract_mapping(schema[key], new_key)
        return data

    def get_backoffice_schema(self):
        """
        Initializes backoffice_data and backoffice_mapping fields from formio schema
        :return: schema of the backoffice form linked to the application service
        """
        form_schema = self.safe_request(f"{self.backoffice_form_url}/schema")
        self.backoffice_data = self.extract_schema(form_schema)
        self.backoffice_mapping = self.extract_mapping(form_schema, "")

    def safe_request(self, url, method="GET", payload=None, authenticated=True):
        """
        Method for handling possible errors that may be encountered in GET a http request
        :param payload:
        :param method:
        :param authenticated:
        :param url: url to request
        :param content: return type is content? Default json
        :return: Response data
        """
        attempt = 0

        # Retry request for #attempts

        headers = {}
        if authenticated:
            headers["Authorization"] = self.token

        if method in ["POST", "PUT"]:
            headers["Content-Type"] = "application/json"

        while attempt < self.attempts:
            logger.debug(f"attempt {attempt + 1}/{self.attempts}: {method}: {url}")
            if payload:
                logger.debug(f"DATA: {json.dumps(payload, indent=2)}")

            attempt += 1
            response = requests.request(
                method=method,
                url=url,
                data=json.dumps(payload),
                headers=headers
            )
            try:
                if response.ok:
                    return response.json()
                elif response.status_code == 401:
                    # If token is expired, request a new one
                    self.set_token()
                elif response.status_code in range(400, 600):
                    raise SDCException(
                        f"SDC: {response.status_code} error while requesting {method} {url}:{json.dumps(response.json())}")
            except JSONDecodeError:
                raise SDCException( f"SDC: Response is not a valid JSON: {response.text}")

            sleep(self.sleep_seconds * 2 ^ attempt)

        raise SDCException(f"SDC: cannot complete request  {method} {url} after {attempt} attempts")

    def get_from_url(self, url):
        """
        Get data from url
        :param url: Requested url
        :return: Requested data
        """
        return self.safe_request(url)

    def get_application(self, application_id):
        """
        Get an application given its identifier
        :param application_id: Application id
        :return: Application data
        """
        url = "{}/applications/{}".format(self.api_url, application_id)
        return self.safe_request(url)

    def update_backoffice_data(self, application_id, payload):
        """
        Updates application backoffice submission
        :param application_id: application identifier
        :param payload: backoffice submission
        :return:
        """
        return self.safe_request(
            url=f"{self.api_url}/applications/{application_id}/backoffice",
            method="PUT",
            payload=payload
        )

    def get_service(self, service_id):
        """
        Get application history given the application identifier
        :param id: application id
        :return: List of application's messages
        """
        url = f"{self.api_url}/services/{service_id}"
        return self.safe_request(url)

    def get_applications(self):
        """
        Method for the recovery of the applications to be registered
       :param offset: SDC api start offset
        :param limit: SDC api page limit
        :return: List of all applocations to be registered
        """
        applications = []

        url = f"{self.api_url}/applications?service={self.service['slug']}&offset={self.api_offset}&limit={self.api_limit}&order=submissionTime&serviceId={self.service_id}"
        if self.start_date:
            url = url + f"&createdAt[after]={self.start_date}"
        if self.end_date:
            url = url + f"&createdAt[before]={self.end_date}"

        while url:
            # Scroll through the API pages to retrieve all the applications
            _applications = self.get_from_url(url)

            # Next page link
            url = _applications["links"]["next"]

            # At least one filter is required
            _application: dict
            for _application in _applications["data"]:
                if not self.ids:
                    # If a list of ids has not been specified, check the status and service of the application
                    if _application["status_name"] not in self.statuses:
                        continue
                elif _application["id"] not in self.ids:
                    # If the list of ids is present, it is assumed that no further filter is needed
                    continue
                applications.append(_application)
                if self.limit and len(applications) == self.limit:
                    return applications

        return applications
