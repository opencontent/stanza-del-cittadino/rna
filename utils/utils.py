import json
import os

import dotenv

dotenv.load_dotenv()
CONFIG_PATH = os.environ.get("CONFIG_PATH", "./config/config.json")

CONFIG = json.load(open(CONFIG_PATH))
DATE_FMT = "%Y-%m-%d"


def merge_dicts(dict1=None, dict2=None):
    """
    Computes a recursive merge of the two input dictionaries
    :param dict1: Base dictionary to merge from
    :param dict2: Dictionary to be merged with the base dictionary
    :return: Dictionary resulting from the merge
    """
    if dict1 is None:
        dict1 = dict()
    if dict2 is None:
        dict2 = dict()
    result_dict = dict1.copy()  # start with keys and values of x

    for key, value in dict2.items():
        if key not in result_dict:
            result_dict[key] = value
        elif type(value) == dict:
            result_dict[key] = merge_dicts(result_dict[key], dict2[key])

    return result_dict


def unflatten_submission(submission_data=None):
    """
    Decompresses the form submission based on its keys
    :param submission_data: form submission data
    :return: decompressed submission
    """
    if submission_data is None:
        submission_data = dict()
    data = dict()
    for k, v in submission_data.items():
        parts = k.split(".")
        tmp_data = data
        for part in parts[:-1]:
            if part not in tmp_data:
                tmp_data[part] = dict()
            tmp_data = tmp_data[part]
        tmp_data[parts[-1]] = v
    return data


registra_aiuto = {
    "caller": CONFIG["CALLER"],
    "codiceBando": CONFIG["COD_BANDO"],
    "INVIO": {
        "COD_BANDO": CONFIG["COD_BANDO"],
        "NOTIFICA_ELABORAZIONE_RICHIESTA": CONFIG["NOTIFICA_ELABORAZIONE_RICHIESTA"],
        "COD_TIPO_VARIAZIONE": CONFIG["COD_TIPO_VARIAZIONE"],
        "CONCESSIONI": {
            "CONCESSIONE": [
                {
                    "TITOLO_PROGETTO": CONFIG["TITOLO_PROGETTO"],
                    "DESCRIZIONE_PROGETTO": CONFIG["DESCRIZIONE_PROGETTO"],
                    "COD_TIPO_INIZIATIVA": CONFIG["COD_TIPO_INIZIATIVA"],
                    "CUP_CHECK_RICHIESTA": CONFIG["CUP_CHECK_RICHIESTA"],
                    "DATA_DOMANDA": CONFIG["DATA_DOMANDA"],
                    "DATA_INIZIO_PROGETTO": CONFIG["DATA_INIZIO_PROGETTO"],
                    "DATA_FINE_PROGETTO": CONFIG["DATA_FINE_PROGETTO"],
                    "ID_CONCESSIONE_GEST": CONFIG["ID_CONCESSIONE_GEST"],
                    "DATA_CONCESSIONE": CONFIG["DATA_CONCESSIONE_PRESUNTA"],
                    "ATTO_CONCESSIONE": CONFIG["ATTO_CONCESSIONE_PRESUNTA"],
                    "CODICE_LOCALE_PROGETTO": CONFIG["CODICE_LOCALE_PROGETTO"],
                    "FLAG_AIUTO_FORZATO": CONFIG["FLAG_AIUTO_FORZATO"],
                    "BENEFICIARIO": {
                        "COD_TIPO_SOGGETTO": CONFIG["COD_TIPO_SOGGETTO"],
                        "CODICE_FISCALE": CONFIG["CODICE_FISCALE"],
                        "DENOMINAZIONE": CONFIG["DENOMINAZIONE"],
                        "FINE_ESERCIZIO_FINANZIARIO": CONFIG["FINE_ESERCIZIO_FINANZIARIO"],
                        "COD_DIMENSIONE_IMPRESA": CONFIG["COD_DIMENSIONE_IMPRESA"],
                        "COD_FORMA_GIURIDICA": CONFIG["COD_FORMA_GIURIDICA"],
                        "COD_TIPO_ATTIVITA_PREVALENTE": CONFIG["COD_TIPO_ATTIVITA_PREVALENTE"],
                        "SEDE_LEGALE": {
                            "INDIRIZZO": CONFIG["INDIRIZZO"],
                            "COMUNE": CONFIG["COMUNE"],
                            "CAP": CONFIG["CAP"],
                            "COD_COMUNE": CONFIG["COD_COMUNE"],
                        },
                    },
                    "LOCALIZZAZIONI": {
                        "LOCALIZZAZIONE": {
                            "ID_LOCA_GEST": CONFIG["ID_LOCA_GEST"],
                            "COD_REGIONE": CONFIG["COD_REGIONE"],
                            "CHECK_LOCALIZZAZIONE": CONFIG["CHECK_LOCALIZZAZIONE"]
                        }
                    },
                    "COSTI": {
                        "COSTO": {
                            "ID_COSTO_GEST": CONFIG["ID_COSTO_GEST"],
                            "COD_TIPO_COSTO": CONFIG["COD_TIPO_COSTO"],
                            "IMPORTO_AMMESSO": CONFIG["IMPORTO_AMMESSO"]
                        }
                    },
                    "COMPONENTI_AIUTI": {
                        "COMPONENTE_AIUTO": {
                            "ID_COMP_AIUTO_GEST": CONFIG["ID_COMP_AIUTO_GEST"],
                            "DESCR_BREVE": CONFIG["DESCR_BREVE"],
                            "COD_TIPO_PROCEDIMENTO": CONFIG["COD_TIPO_PROCEDIMENTO"],
                            "CODICE_REGOLAMENTO": CONFIG["CODICE_REGOLAMENTO"],
                            "COD_OBIETTIVO": CONFIG["COD_OBIETTIVO"],
                            "CODICE_SETTORE": CONFIG["CODICE_SETTORE"],
                            "CUMULABILITA": CONFIG["CUMULABILITA"],
                            "FLAG_CE": CONFIG["FLAG_CE"],
                            "CODICE_CE": CONFIG["CODICE_CE"],
                            "COD_ATECO_LIST": {
                                "COD_ATECO": CONFIG["COD_ATECO"]
                            },
                            "STRUMENTI_AIUTO": {
                                "STRUMENTO_AIUTO": {
                                    "ID_STRUM_AIUTO_GEST": CONFIG["ID_STRUM_AIUTO_GEST"],
                                    "COD_TIPO_STRUMENTO_AIUTO": CONFIG["COD_TIPO_STRUMENTO_AIUTO"],
                                    "IMPORTO_NOMINALE": CONFIG["IMPORTO_NOMINALE"],
                                    "IMPORTO_AGEVOLAZIONE": CONFIG["IMPORTO_AGEVOLAZIONE"],
                                    "INTENSITA_AIUTO": CONFIG["INTENSITA_AIUTO"],
                                }
                            }
                        }
                    }
                }
            ]
        }
    }
}

stato_esito_richiesta = {
    "caller": CONFIG["CALLER"],
    "idRichiesta": CONFIG["ID_RICHIESTA"]
}

conferma_concessione = {
    "caller": CONFIG["CALLER"],
    "codiceBando": CONFIG["COD_BANDO"],
    "INVIO_CONFERMA_CONCESSIONI": {
        "COD_BANDO": CONFIG["COD_BANDO"],
        "NOTIFICA_ELABORAZIONE_RICHIESTA": CONFIG["NOTIFICA_ELABORAZIONE_RICHIESTA"],
        "CONFERMA_CONCESSIONI": {
            "CONFERMA_CONCESSIONE": [
                {
                    "COR": CONFIG["COR"],
                    "ATTO_CONCESSIONE": CONFIG["ATTO_CONCESSIONE"],
                    "DATA_CONCESSIONE": CONFIG["DATA_CONCESSIONE"]
                }
            ]
        }
    }
}
