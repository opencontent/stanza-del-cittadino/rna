# RNA integration with "Stanza del Cittadino"

Batch application for the integration of "Stanza del Cittadino" with the National Business Register (RNA)

## Description
The application is responsible for querying the apis of the SDC and, based on the current state of the individual 
applications, to make the appropriate requests for the registration of economic aid to the RNA.

## Workflow

### Retrieval
The process begins with the recovery of the applications via API: during the configuration phase it is possible to 
filter all the applications relating to a single service by date, by identifier or by current status.

The service to which the applications belong must be linked to a backoffice with the only constraint that among the 
fields configured in the form there is the nested form "rna" with key "rna".

Le applications verranno successivamente filtrate sulla base dei valori inseriti nel form backoffice:

![](screenshots/rna_backoffice.png)

The first boolean flag "Inviare la richiesta a RNA?" indicates whether or not the request for economic aid should be 
forwarded to the national register, all applications that do not have this flagged value will consequently be ignored:
in case of any error during the process the application will take care of disabling the sending.

The "Stato della richiesta" field represents the status of the integration at each step:

0. **Aiuto da registrare**: the request for registration of economic aid has not yet been submitted
1. **In attesa di esito della richiesta di aiuto**: the registration request has been sent, the application is awaiting the outcome
2. **Richiesta di aiuto completata**: The result of the request has been obtained, if successful the COR field will be populated with the data obtained from the outcome download, otherwise a text field will be displayed populated with the errors found.
3. **Concessione da confermare**: If there are no errors, the submission of the concession request is pending.
4. **In attesa di esito della conferma di contributo**: The request for confirmation of the concession has been sent. The outcome of the request is awaited.
5. **Conferma concessione completata**: The concession has been confirmed, no further action is required

## Configuration

To configure the application, several parameters are required, they will be listed below divided by area:

### General Parameters

|   Name                |   Description                                                 |   Default                         |
|-----------------------|---------------------------------------------------------------|-----------------------------------|
|   `LOG_LEVEL`         |   Application log level (`INFO`, `DEBUG`, `ERROR`, `WARN`)    |   `INFO`                          |
|   `CONFIG_PATH`       |   Path of the configuration file                              |   `./config/config.json`          |


### RNA Parameters

|   Name                                    |   Description                                                                     |   Default                 |   Required    |
|-------------------------------------------|-----------------------------------------------------------------------------------|---------------------------|---------------|
|   `RNA_USERNAME`                          |   Username used for basic auhentication                                           |   --                      |   `true`      |
|   `RNA_PASSWORD`                          |   Password used for basic auhentication                                           |   --                      |   `true`      |
|   `RNA_ENDPOINT`                          |   RNA API's endpoint                                                              |   --                      |   `true`      |
|   `RNA_CERTIFICATE`                       |   Certificate used for authentication                                             |   `./config/cert.pem`     |   `true`      |
|   `RNA_ATTEMPTS`                          |   Number of retries in case of error                                              |   10                      |   `false`     |
|   `RNA_ERROR_SLEEP_SECONDS`               |   Number of seconds to wait in case of an error                                   |   60                      |   `false`     |
|   `RNA_SUCCESS_SLEEP_SECONDS`             |   Number of seconds to wait in case of success                                    |   1                       |   `false`     |
|   `RNA_REGISTRA_AIUTO_SLEEP_SECONDS`      |   Number of seconds to wait at the end of an economic aid registration request    |   4                       |   `false`     |

### SDC Parameters

|   Name                                    |   Description                                                                     |   Default                                     |   Required    |
|-------------------------------------------|-----------------------------------------------------------------------------------|-----------------------------------------------|---------------|
|   `SDC_USERNAME`                          |   Username used for API's auhentication                                           |   --                                          |   `true`      |
|   `SDC_PASSWORD`                          |   Password used for API's auhentication                                           |   --                                          |   `true`      |
|   `SDC_ENDPOINT`                          |   SDC API's endpoint                                                              |   --                                          |   `true`      |
|   `SDC_SERVICE_ID`                        |   SDC service identifier                                                          |   --                                          |   `true`      |
|   `SDC_BACKOFFICE_URL`                    |   Url of the form used in the backoffice                                          |   --                                          |   `true`      |
|   `SDC_STATUSES`                          |   List of application statuses separated by &#124;, all others will be ignored    |   `status_submitted`&#124;`status_pending`    |   `true`      |
|   `SDC_API_OFFSET`                        |   Initial API's offset                                                            |   0                                           |   `false`     |
|   `SDC_API_LIMIT`                         |   Number of results per page                                                      |   100                                         |   `false`     |
|   `SDC_IDS`                               |   List of application ids separated by &#124;, all others will be ignored         |   --                                          |   `false`     |
|   `SDC_START_DATE`                        |   Filter applications by submission date                                          |   --                                          |   `false`     |
|   `SDC_END_DATE`                          |   Filter applications by submission date                                          |   --                                          |   `false`     |
|   `SDC_APPLICATIONS_LIMIT`                |   Limits the number of applications to be sent to RNA                             |   --                                          |   `false`     |
|   `SDC_ATTEMPTS`                          |   Number of retries in case of error                                              |   10                                          |   `false`     |
|   `SDC_ERROR_SLEEP_SECONDS`               |   Number of seconds to wait in case of an error                                   |   60                                          |   `false`     |
|   `DEFAULT_PLACEHOLDERS_PATH`             |   Path of the placeholder defaults file                                           |   `./config/default_placeholders.json`        |   `false`     |


## Execution

### Terminal

1. Install virtualenv

        virtualenv venv --python python3.7
        source venv/bin/python

2. Intstall requirements

        pip install -r requirements.txt

3. Execution

        python -W ignore rna.py

### Docker

        docker build -t rna .
        docker run --name rna --rm rna

    

